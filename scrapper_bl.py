import bs4
import lxml
import re
from selenium import webdriver

from tqdm import tqdm
import random
import time
from datetime import datetime

import requests
from PIL import Image
import os
import json

from tqdm import tqdm
from utils import PrintException, Random_uid

chrome_options = webdriver.ChromeOptions()
# myProxy = '175.106.17.62:47641'
# chrome_options.add_argument('--proxy-server=%s' % myProxy) 
# chrome_options.add_argument('--headless') #for pop up windows
chrome_options.add_argument("user-data-dir=cache/selenium")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-setuid-sandbox')

chrome_options.add_experimental_option("prefs", {
  "download.default_directory": r"/run/media/abdul/Asus/ICU/autoMarketplace",
  "download.prompt_for_download": False,
  "download.directory_upgrade": True,
  "safebrowsing.enabled": True
})

def download_img(link, root='./image/bukalapak/'):
    randomUid = Random_uid()
    list_link = link.split('/')
    name_file =  'bl_img' + randomUid.get_int() + list_link[-1]
    response = requests.get(link)
    #time.sleep(1)
    filename = root+name_file
    file = open(filename, "wb")

    file.write(response.content)

    file.close()
    
    print("Succes download %s" %(filename))
    
    return filename

def parser_bl(soup):
    clean_list = []
    
    
    raw_prod_terlaris = soup.find('div',{'class':'bl-container loop--recommendations__item'})
    if type(raw_prod_terlaris) is not type(None):
        list_raw_prod_terlaris = raw_prod_terlaris.findAll('div',{'class':'bl-carousel__slide loop--recommendations__carousel'})
    
        for raw_terlaris in list_raw_prod_terlaris:
            try:
                dict_data = {
                    'url' : None,
                    'url_img_thumb' : None,
                    'prod_name' : None,
                    'price_real' : 0,
                    'disc': 0,
                    'price_now':0,
                    'rating': 0,
                    'n_sold': 0
                }

                tumbnail_raw = raw_terlaris.find('div',{'class':'bl-product-card__thumbnail'})
                url_prod = tumbnail_raw.find('a').get('href')
                dict_data['url'] = url_prod
                url_img = tumbnail_raw.find('img').get('src')
                dict_data['url_img_thumb'] = url_img

                prod_desc_raw = raw_terlaris.find('div', {'class':'bl-product-card__description'})
                prod_name = prod_desc_raw.find('div',{'class':'bl-product-card__description-name'}).text
                dict_data['prod_name'] = prep_txt(prod_name)

                price_raw = prod_desc_raw.find('div', {'class':'bl-product-card__description-price'})
                price_now = price_raw.find('p').text
                dict_data['price_now'] = prep_number(price_now)
                try:
                    price_disc_raw = price_raw.find('div', {'class':'bl-product-card__description-price-discount'})
                    price = price_disc_raw.find('span',{'style':'text-decoration: line-through;'}).text
                    dict_data['price_real'] = prep_number(price)
                    disc_price = price_disc_raw.find('span',{'class':'bl-text bl-text--caption bl-text--error'}).text
                    dict_data['disc'] = prep_txt(disc_price)
                except:
                    pass

                reputation_raw =  prod_desc_raw.find('div', {'class':'bl-product-card__description-rating-and-sold'})
                rep_list = reputation_raw.findAll('p', {'class':'bl-text bl-text--body-small bl-text--subdued'})
                rating = reputation_raw.find('div', {'class':'bl-product-card__description-rating'}).find('a').text
                dict_data['rating'] = prep_number(rating)
                if len(rep_list) >= 2 :
                    n_sold = rep_list[-1].text
                    dict_data['n_sold'] = prep_number(n_sold)
                
                clean_list.append(dict_data)
            except:
                PrintException()
                continue
    
    return clean_list

def scroll_page(driver):
    data = []
    driver.get('https://bukalapak.com')
    time.sleep(1)
    last_height = driver.execute_script("return document.body.scrollHeight")
    
    i=1
    height_start = 0 
    height_target = 0
    while True:
        print('Scrolling.... {}x'.format(i))

        # Scroll down to bottom
        height = driver.execute_script("return document.body.scrollHeight")
        height_target = height_target + int(height/10)
        #driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        driver.execute_script("window.scrollTo({}, {});".format(height_start, height_target))
     
        # Wait to load page
        rand = random.randint(5,7)
        time.sleep(rand)

        # Calculate new scroll height and compare with last scroll height
        #new_height = driver.execute_script("return document.body.scrollHeight")
        if height_start >= last_height:
            print('Done Scrolling')
            break
#         last_height = new_height
        height_start = height_target
        #print('\n')
        i=i+1
        
        soup = bs4.BeautifulSoup(driver.page_source,'lxml')
        data = parser_bl(soup)
        try:
            if len(data) >=1:
                print('Sucess getting data')
                return driver, data
        except :
            PrintException()
            continue
            
    return driver, data

def prep_ulasan(text):
    text = re.sub(r"\D+",r"",text)
    return text

def prep_number(text):
    ls_delete = ['Rp',' ','Terjual','\n','.']
    for trsh in ls_delete:
        text = text.replace(trsh,'')
    return int(text)

def prep_txt(text):
    text =  re.sub(r'([^A-Za-z0-9 -])|(  )|(\bn)|(n\b$)', r'', text)
    
    return text

def parser_post_product(soup):
    dict_product = {
        'url_user': 'bukalapak.com',
        'kondisi': 'Baru',
        'deskripsi': None,
        'ulasan': 0,
        'Kategori': None,
        'Berat':0,
        'Asal Barang':'Impor',
        'list_img':None
    }
    try:
        raw_user_header = soup.find('div',{'class':'c-seller__header'})
        url_user = raw_user_header.find('a').get('href')
        dict_product['url_user'] =  url_user
        raw_reviews = soup.find('div',{'class':'c-main-product__reviews'})

        raw_det_barang = soup.find('div', {'id':'section-informasi-barang'})

        raw_barang = raw_det_barang.find('div',{'class':'c-product-details-section__main'})
        kondisi = raw_barang.find('div',{'class':'c-label'}).text
        dict_product['kondisi'] = kondisi
        raw_info_table = raw_barang.find('table',{'class':'c-information__table'})

        list_spek_raw = raw_info_table.findAll('tr')
        dict_spek = {}
        for ls in list_spek_raw:
            keys = ls.find('th').text
            values = ls.findAll('td')[1].text
            dict_spek[keys] = values

        dict_product.update(dict_spek)
        dict_product['Berat'] = prep_ulasan(dict_product['Berat'])
        deskripsi = soup.find('div',{'class':'c-information__description-txt'}).text
        dict_product['deskripsi'] = deskripsi
        ulasan_raw = raw_reviews.find('div',{'class':'c-main-product__rating u-mrgn-right--2'}).text
        dict_product['ulasan'] = prep_ulasan(ulasan_raw)

        list_img = []
        img_list_raw = soup.findAll('div',{'class':'c-product-gallery__thumbnail'})
        for img_ in img_list_raw:
            url_img_bl = img_.find('img').get('src')
            img_name = download_img(url_img_bl)
            list_img.append(img_name)
        dict_product['list_img'] = list_img
    except:
        PrintException()

    return dict_product

def scrapper_bl(options):
    driver = webdriver.Chrome(options=options)
    driver, data = scroll_page(driver)
    new_data = []
    for a_data in tqdm(data):
        try:
            driver.get(a_data['url'])
            time.sleep(2)
            print(a_data['url'])
            soup = bs4.BeautifulSoup(driver.page_source,'lxml')
            prod = parser_post_product(soup)
            a_data.update(prod)
            new_data.append(a_data)
        except:
            PrintException()
            continue
    return driver, new_data
    