from selenium import webdriver
import time
import bs4

login_options = webdriver.ChromeOptions()
login_options.add_argument("user-data-dir=cache/selenium")
# chrome_options.add_argument('--headless') #for pop up windows

check_options = webdriver.ChromeOptions()
check_options.add_argument("user-data-dir=cache/selenium")
# check_options.add_argument('--headless') #for pop up windows


def login(options):
    print('Login tokopedia...\n')
    driver = webdriver.Chrome(options=options)
    time.sleep(1)

    driver.get('https://www.tokopedia.com/login')

    iterations  = 0
    while True:
        time.sleep(1)
        if iterations == 1000:
            print("Timeout for login check your connections")

        soup = bs4.BeautifulSoup(driver.page_source,'lxml')
        try:
            user = soup.find('div', {'id':'my-profile-header'}).text
            print('Login successfully welcome %s' % (user))
            driver.close()
            break
        except:
            iterations = iterations +1
            continue

def checkaccount(options):
    result = 0

    print('Checking account...\n')
    driver = webdriver.Chrome(options=options)
    time.sleep(1)

    driver.get('https://www.tokopedia.com/')
    time.sleep(1)
    soup = bs4.BeautifulSoup(driver.page_source,'lxml')
    try:
        user = soup.find('div', {'id':'my-profile-header'}).text
        print('Your login as  %s' % (user))
        result = 1
    except:
        print('Your not login for any account tokopedia')
        pass
    driver.close()

    return result
    
if __name__ == '__main__':
    val = input("Choose your options : \n\t0 -> login\n\t1 -> check login\n ") 
    
    if int(val) == 0:
        login(options = login_options)
    else:
        checkaccount(options = check_options)
    
    exit()