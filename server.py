# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings('ignore')

########################## Sanic##################################
# Sanic
import asyncio
# import uvloop
from sanic import Sanic
from sanic import response
from sanic.response import json
from sanic.request import RequestParameters
from sanic.response import text
from sanic.exceptions import ServerError
from sanic_openapi import swagger_blueprint#, openapi_blueprint
from sanic_openapi import doc
from sanic_cors import CORS, cross_origin

# STD LIB
import os
import base64
import gc
import time
import sys
from datetime import datetime
import argparse

from dbtools import sql_get_alltokped, sql_get_alltask, sql_get_edittask

# Configuration
##API
parser = argparse.ArgumentParser(description='PORT')
parser.add_argument('--port', type=int, default=3007, help='port')

args = parser.parse_args()

print(">> Launching AUTO SCRAPPING ENGINE")
print(">> Coded by: Abdul Hamid")
boottime1 = time.time()

port = args.port
    

# asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
app = Sanic(name='AutoMarketplaceAPP')
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config["CORS_AUTOMATIC_OPTIONS"] = True
# app.blueprint(openapi_blueprint)
app.blueprint(swagger_blueprint)
app.config.API_VERSION = "0.0.0"
app.config.API_TITLE= "AUTOMARKETPLACE API SERVER: " , port
app.config.API_TERMS_OF_SERVICE= "null"
app.config.API_CONTACT_EMAIL="amitabduls@gmail.com"

# Data Model untuk Swagger
class db_type:
    db_type = str

class db_loc:
    db_loc = str

class edittask:
    markup = str
    upload = bool
    path = str

############################################################################## Model Hamid (2 kelas) #####################################################################

def logging(message):
    message_format = "[{}] [INFO]-{}".format(datetime.now(), message)
    print(message_format)

def get_db_list(db_type='tokped'):
    data = {}
    if db_type == 'tokped':
        for root, dirs, files in os.walk("db/tokped", topdown=False):
            for name in files:
                path_db = os.path.join(root, name)
                data[name] = path_db

    return data

@app.route("/api/get_db_list/", methods=["POST", "OPTIONS"])
@doc.summary("Synchronous Auto Marketplace payload")
@doc.consumes(db_type, location="body")
async def sync_dblist(request):
    try:
        body = request.json
    except OSError as er:
        raise ServerError("Not valid JSON", status_code=400)
    try:
        starting = time.time()

        db_type = body.get('db_type')
        list_db = get_db_list(db_type)
        ending = time.time()
        # print("\nTime Predict end to end {}\n".format(ending-starting))
        logging("get db list succes with {}s".format(ending-starting))
        return response.json(list_db)
    except OSError as er:
        raise ServerError("OSError occured", status_code=500)

@app.route("/api/db/", methods=["POST", "OPTIONS"])
@doc.summary("Synchronous Auto Marketplace payload")
@doc.consumes(db_loc, location="body")
async def sync_db_show(request):
    try:
        body = request.json
    except OSError as er:
        raise ServerError("Not valid JSON", status_code=400)
    try:
        starting = time.time()

        db_loc = body.get('db_loc')
        db_show = sql_get_alltokped(db_loc)
        ending = time.time()
        time_process = ending-starting
        result = {
            'data' : db_show, 'size':len(db_show),'time':time_process
        }
        print(result)
        logging("get db data succes with {}s".format(ending-starting))
        return response.json(result)
    except OSError as er:
        raise ServerError("OSError occured", status_code=500)

@app.route("/api/task/", methods=["GET", "OPTIONS"])
@doc.summary("Synchronous Auto Marketplace payload")
async def sync_alltask(request):
    try:
        starting = time.time()

        db_show = sql_get_alltask()
        ending = time.time()
        time_process = ending-starting
        result = {
            'data' : db_show, 'size':len(db_show),'time':time_process
        }
        # print(result)
        logging("get db data succes with {}s".format(ending-starting))
        return response.json(result)

    except OSError as er:
        raise ServerError("OSError occured", status_code=500)

@app.route("/api/task/edit", methods=["POST", "OPTIONS"])
@doc.summary("Synchronous Auto Marketplace payload")
@doc.consumes(edittask, location="body")
async def sync_edittask(request):
    try:
        body = request.json
    except OSError as er:
        raise ServerError("Not valid JSON", status_code=400)

    try:
        path = body.get('path')
        markup = body.get('markup')
        upload = body.get('upload')
        values_edit = (
            markup, upload , path
        )

        starting = time.time()

        res = sql_get_edittask(values=values_edit)
        ending = time.time()
        time_process = ending-starting
        if res == 1:
            result = {
                'message' : 'succes edit data %s'%(path),'time':time_process
            }
        else:
            result = {
                'message' : 'failed edit data %s'%(path),'time':time_process
            }
        # print(result)
        logging("{} {}s".format(result['message'],ending-starting))
        return response.json(result)

    except OSError as er:
        raise ServerError("OSError occured", status_code=500)


boottime2 = time.time()
boottime = boottime2 - boottime1
print(">> AUTO MARKETPLACE  Engine boot time: ", boottime, " seconds")
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=port, workers=1, auto_reload=True)
