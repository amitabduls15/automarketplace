from PIL import Image
import psutil
import linecache
import sys
import requests
import base64
import os

import uuid, shortuuid
import openpyxl
from tqdm import tqdm

KEY = 'd16cf2dab13d42392e2793fb5df27190'

def prepocessing_number(txt):
    txt = str(txt)
    new_txt = txt.replace('.','').replace('Rp','').replace('(','').replace(')','')
    new_txt = new_txt.replace(',','.')
    if 'rb' in new_txt:
        new_txt = new_txt.replace('rb','')
        float_number = float(new_txt) * 1000
    else:
        float_number = float(new_txt)
    return float_number

def download_img(link, root='./image/',resolusi=100):
    res_str = '{}-square'.format(resolusi)
    link = link.replace('100-square',res_str)
    list_link = link.split('/')
    name_file = list_link[-1]
    if '?' in name_file:
        name_file = name_file.split('?')[0]
    response = requests.get(link)
    #time.sleep(1)
    filename = root+name_file
    file = open(filename, "wb")

    file.write(response.content)

    file.close()
    
    im = Image.open(filename).convert('RGB')
    filename_png = filename.replace('webp', 'png')
    im.save(filename_png, 'png')
    os.remove(filename)
    
    print("Succes download %s" %(filename))
    
    return filename_png

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print ('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

def upload_img_b64(key, base64img):
    url = 'https://api.imgbb.com/1/upload'
    myobj = {'key': key,
            'image':base64img}

    x = requests.post(url, data = myobj)

    return x.json()

def upload_img(key, path):
    try:
        with open(path, "rb") as img_file:
            base64img = base64.b64encode(img_file.read())
    except Exception as err:
        raise err

    res_json = upload_img_b64(key=key, base64img=base64img) 
    #print(res_json)
    if res_json['status']==200:
        filename_ori = path.split('/')[-1]
        root = path.replace(filename_ori, '')
        path_filename_new = root + res_json['data']['image']['filename']
        os.rename(path, path_filename_new)

        return res_json['data']['url']

def mass_upload_img(list_img, key=KEY):
    list_url = []
    for i in range(5):
        try:
            url_img = upload_img(key=key, path=list_img[i])
            list_url.append(url_img)
        except:
            list_url.append('NULL')
            continue
    return list_url

class Random_uid:
    def __init__(self):
        shortuuid.set_alphabet("abcdefghijklmnopqrstuvwxyz")
        self.sku = shortuuid.uuid()[:5]

    def refresh_sku(self):
        self.sku = shortuuid.uuid()[:5]
    
    def get_int(self, lenInt=10):
        uid = uuid.uuid4()
        int_uid = str(uid.int)[:lenInt]
        return int_uid
    
    def get_sku_name(self):
        uid = uuid.uuid4()
        int_uid = str(uid.int)[:10]
        
        return self.sku +'-'+ int_uid

def create_xlsx(workbook_path, data_product, output_path,markup=1000):
    wb = openpyxl.load_workbook('./example/template_tokped.xlsx')
    # print(wb.sheetnames)
    ws=wb.get_sheet_by_name('ISI Template Impor Produk')
    i = 0
    for data in tqdm(data_product, desc='Input Data'):
        j = 0
        for a_cell in data:
            if a_cell != 'NULL':
                if j==19:
                    # print(a_cell)
                    ws.cell(row=4 + i , column=2 + j, value=a_cell+markup)
                else:
                    ws.cell(row=4 + i , column=2 + j, value=a_cell)
            j = j +1
        i = i + 1
    
    wb.save(filename = output_path)

    print('Succes create excell file to %s' % (output_path))