import bs4
import lxml
from bs4 import BeautifulSoup
from selenium import webdriver

from tqdm import tqdm
import random
import time
from datetime import datetime

import psutil
import linecache
import sys
from pprint import pprint

from utils import prepocessing_number, download_img, PrintException




def parser_header(soup, root='./image/'):
    dict_product_content = {
        'product_name' : None,
        'n_stock':0,
        'n_terjual': 0,
        'n_stars': 0,
        'n_reviews':0,
        'n_discuss':0,
        'product_price':0,
        'list_img_link':None,
        'list_img':None
    }
    try:
        stock_raw = soup.find('div', {'data-testid':'quantityOrder'}).text.split(' ')[1]
        stock = prepocessing_number(stock_raw)
        dict_product_content['n_stock'] = stock
        
        image_list_raw = soup.findAll('div', {'data-testid':'PDPImageThumbnail'})
        image_list_link = [img.find('img').get('src') for img in image_list_raw]
        dict_product_content['list_img_link'] = image_list_link
        
        list_img = []
        # print(image_list_link)
        for link_img in image_list_link:
            filename_img = download_img(link=link_img ,root=root,resolusi=500)
            list_img.append(filename_img)
        dict_product_content['list_img'] = list_img
    
        header_raw = soup.find('div', {'id':'pdp_comp-product_content'})
        product_name = header_raw.find('h1',{'data-testid':'lblPDPDetailProductName'}).text
        dict_product_content['product_name'] = product_name
        # product_name

        items_raw = header_raw.find('div', {'class':'items'})
        # items_raw
        n_terjual = items_raw.find('div', {'data-testid':'lblPDPDetailProductSoldCounter'}).text.replace('Terjual ','').replace('.','')
        dict_product_content['n_terjual'] = prepocessing_number(n_terjual)
        # n_terjual

        n_stars = float(items_raw.find('span', {'data-testid':'lblPDPDetailProductRatingNumber'}).text)
        dict_product_content['n_stars'] = n_stars
        # n_stars

        n_reviews = items_raw.find('span', {'data-testid':'lblPDPDetailProductRatingCounter'}).text.replace('.','').replace('(','').replace(')','').replace(' ulasan','')
        dict_product_content['n_reviews'] = prepocessing_number(n_reviews)
        # n_reviews

        n_discuss = int(items_raw.find('div', {'data-testid':'lblPDPDetailProductDiscussionNumber'}).text.replace('.','').replace('(','').replace(')','').replace('Diskusi',''))
        dict_product_content['n_discuss'] = prepocessing_number(n_discuss)
        # n_discuss

        product_price = int(header_raw.find('div', {'data-testid':'lblPDPDetailProductPrice'}).text.replace('.','').replace('Rp',''))
        dict_product_content['product_price'] = prepocessing_number(product_price)
        # product_price
    
    except:
        PrintException()
        pass
    
    return dict_product_content



def parser_detail_product(soup):
    dict_detail = {
        'Kondisi': None,
        'Berat' : None,
        'Kategori' : None,
        'Etalase' : None,
        'description' : None
    }
    
    try:
        detail_product_raw = soup.find('div',{'id':'pdp_comp-product_detail'})
        info_product_raw = detail_product_raw.find('ul',{'data-testid':'lblPDPInfoProduk'})
        list_info_raw = info_product_raw.findAll('li')
        info_dict = {}
        for info_raw in list_info_raw:
            info_txt = info_raw.text
            info_list = info_txt.split(': ')
            info_dict[info_list[0]]=info_list[1]
        dict_detail.update(info_dict)
        #info_dict    
        dict_detail['Berat'] = dict_detail['Berat'].replace('.','').replace(' Gram','')
        if 'Kilogram' in dict_detail['Berat']:
            dict_detail['Berat'] = int(dict_detail['Berat'].replace(' Kilogram','')) * 1000
        description_raw = detail_product_raw.find('div',{'data-testid':'lblPDPDescriptionProduk'})
        description = str(description_raw).replace('<div data-testid="lblPDPDescriptionProduk">','').replace('</div>','').replace('<br/>','\n')
        dict_detail['description']=description
        #description
    except:
        PrintException()
        pass
    
    return dict_detail

def scroll_page(driver):
    last_height = driver.execute_script("return document.body.scrollHeight")


    i=1

    while True:
        print('Scrolling.... {}x'.format(i))

        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # Wait to load page
        rand = random.randint(10,12)
        time.sleep(rand)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            print('Done Scrolling')
            break
        last_height = new_height

        #print('\n')
        i=i+1
    
    return driver




def parser_product(soup):
    master_product = soup.findAll('div', {'class':'css-wlcnlb','data-testid':'master-product-card'} ,recursive=True)
    
    if len(master_product)==0:
        return 0
    
    all_product = []
    
    report = {
        'succes':0,
        'failed':0
    }
    
    for product in tqdm(master_product, desc='parsing_product'):
        data_product = {
                        'link':None,
        
                        'img_link':None,
                        'img_alt':None,

                        'product_name':None,
                        'product_price':0,

                        'n_stars' :0,
                        'n_reviews' :0

                        }
        
        try:
        
            link = product.find('a').get('href')
            data_product['link'] = link

            img_detail = product.find('div', {'data-testid':'imgProduct'})
            #rint(img_detail)
            img_link = img_detail.find('img').get('src')
            data_product['img_link'] = img_link
            img_alt = img_detail.find('img').get('alt')
            data_product['img_alt'] = img_alt

            product_name = product.find('div',{'data-testid':'linkProductName'}).text
            data_product['product_name'] = product_name
            product_price = product.find('div',{'data-testid':'linkProductPrice'}).text
            data_product['product_price'] = prepocessing_number(product_price)

            rating = product.find('div',{'data-testid':'linkProductRating'})
            stars = rating.findAll('img')
            n_stars = 0
            for star in stars:
                if star.get('alt') == 'star':
                    n_stars= n_stars +1
                else:
                    n_stars= n_stars +0.5
            data_product['n_stars'] = n_stars
            n_reviews = product.find('span', {'data-testid':'spanReview'}).text
            data_product['n_reviews'] = prepocessing_number(n_reviews)
            
            #pprint(data_product)
            all_product.append(data_product)
            
            report['succes'] = report['succes'] + 1
            
        except:
            PrintException()
            report['failed'] = report['failed'] + 1
            continue
        
    print('Report parsing_product:\n\t succes = {}\n\t failed = {}'.format(report['succes'], report['failed']))
    all_product = list({v['link']:v for v in all_product}.values())
    print('Deleting Duplicate ....')
    
    
    print('before = {}\n after = {}\n\t'.format(report['succes'], len(all_product)))
    return all_product

def scrape_terlaris(options, root='./image/'):
    print('Getting Data Product Best Seller...\n')
    driver = webdriver.Chrome(options=options)
    time.sleep(1)
    
    driver.get('https://www.tokopedia.com/discovery/produk-terlaris')
    time.sleep(2)
    driver = scroll_page(driver)
    time.sleep(3)
    
    soup = bs4.BeautifulSoup(driver.page_source,'lxml')
    
    result = parser_product(soup)
    
    results = []
    for res in tqdm(result, desc = 'detail scrapping'):
        link = res['link']
        driver.get(link)
        time.sleep(2)
        
        soup = bs4.BeautifulSoup(driver.page_source,'lxml')
        header_product = parser_header(soup ,root=root)
        detail_product = parser_detail_product(soup)
        res.update(header_product)
        res.update(detail_product)
        
        results.append(res)
        
    
    driver.close()
    
    return results

def bulk_upload(options ,path_data):
    driver = webdriver.Chrome(options=options)
    time.sleep(1)
    driver.get('https://seller.tokopedia.com/bulk/add')
    time.sleep(5)
    
    element = driver.find_element_by_xpath('//*[@id="BulkUploadArea"]/div[3]/input')
    element.send_keys(path_data)
    time.sleep(5)
    
    elemntupload = driver.find_element_by_xpath('//*[@id="BulkUploadArea"]/div[3]/div[3]/button[2]')
    elemntupload.click()
    time.sleep(5)
    
    alt_status = ['Fail Icon', 'Warning Icon', 'Success Icon']
    progress_bar_cache = '0%'

    while True:
        soup = bs4.BeautifulSoup(driver.page_source,'lxml')
        upload_field = soup.find('section',{'id':'BulkUploadArea'})
        try:
            progress_bar = soup.find('div', {'id':'progressbar_bulk'}).text
            if progress_bar_cache != progress_bar:
                print('Upload Progress %s' % (progress_bar))
                progress_bar_cache = progress_bar
            continue

        except:
            time.sleep(1)
            soup = bs4.BeautifulSoup(driver.page_source,'lxml')
            upload_field = soup.find('section',{'id':'BulkUploadArea'})
            #print(upload_field)
            
            alt_img = upload_field.find('img').get('alt')
            
            if alt_img in alt_status:
                message = upload_field.find('div', {'class':'css-1s0chwd'}).text
                suggestion = upload_field.find('div', {'class':'css-1fxly6l'}).text
                print(message)
                print(suggestion)
                time.sleep(5)
                download_element = driver.find_element_by_xpath('//*[@id="BulkUploadArea"]/div[3]/div[3]/button[2]')
                download_element.click()
                time.sleep(5)
                driver.close()
                print('Download succes for revision %s' % (path_data.replace('tokopedia_', 'revisi-tokopedia_')))
                break 
            else:
                continue

if __name__ == '__main__':
    import json

    chrome_options = webdriver.ChromeOptions()
    # myProxy = '175.106.17.62:47641'
    # chrome_options.add_argument('--proxy-server=%s' % myProxy) 
    # chrome_options.add_argument('--headless') #for pop up windows
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-setuid-sandbox')


    hasil = scrape_terlaris(options=chrome_options)
    dict_hasil = {
        str(datetime.now()) : hasil
    }
    with open('tokopedia_results.json', 'w') as json_file:
        json.dump(dict_hasil, json_file)