import sqlite3

def create_db(name_db, db='tokped'):
    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    curr = conn.cursor() # The database will be saved in the location where your 'py' file is saved
    
    if db == 'tokped':
        curr.execute(
            """CREATE TABLE "template_tokped"(
                            "nama_produk" Text NOT NULL,
                            "deskripsi_produk" Text,
                            "kategori_kode" Integer NOT NULL,
                            "berat_gram" Integer NOT NULL,
                            "minimum_pesanan" Integer NOT NULL,
                            "nomor_etalase" Integer,
                            "waktu_proses_po" Integer,
                            "kondisi" Text NOT NULL,
                            "link_gambar1" Text NOT NULL,
                            "link_gambar2" Text,
                            "link_gambar3" Text,
                            "link_gambar4" Text,
                            "link_gambar5" Text,
                            "url_video1" Text,
                            "url_video2" Text,
                            "url_video3" Text,
                            "sku_name" Text,
                            "status" Text NOT NULL,
                            "jumlah_stok" Numeric NOT NULL,
                            "harga" Numeric NOT NULL,
                            "asuransi_pengiriman" Numeric,
                            "link" Text NOT NULL PRIMARY KEY,
                            CONSTRAINT "unique_link" UNIQUE ( "link" ) );"""
        )
        
        curr.execute(
            """CREATE TABLE "product_detail"(
                            "link" Text NOT NULL PRIMARY KEY,
                            "n_stars" Real,
                            "n_reviews" Integer,
                            "n_stock" Integer,
                            "n_terjual" Integer,
                            "n_discuss" Integer,
                            "tanggal" Date,
                            CONSTRAINT "lnk_template_tokped_product_detail" FOREIGN KEY ( "link" ) REFERENCES "template_tokped"( "link" )
                            );"""
        )
    else:
        curr.execute(
            """CREATE TABLE "template_bl"(
                        "nama_barang" Text,
                        "stok" Integer,
                        "berat" Integer,
                        "harga" Integer,
                        "kondisi" Text,
                        "deskripsi" Text,
                        "asuransi" Text,
                        "pengiriman" Text,
                        "url_gambar1" Text,
                        "url_gambar2" Text,
                        "url_gambar3" Text,
                        "url_gambar4" Text,
	                    "url_gambar5" Text,
                        "kode_sku" Text,
                        "link" Text NOT NULL PRIMARY KEY,
                    CONSTRAINT "unique_link" UNIQUE ( "link" ) );"""
        )
        
        curr.execute(
            """CREATE TABLE "detail_produk"(
                    "rating" Real,
                    "n_sold" Numeric,
                    "url_user" Text,
                    "kondisi" Text,
                    "ulasan" Integer,
                    "kategori" Text,
                    "berat" Integer,
                    "asal_barang" Text,
                    "link" Text NOT NULL PRIMARY KEY,
                    CONSTRAINT "lnk_template_bl_detail_produk" FOREIGN KEY ( "link" ) REFERENCES "template_bl"( "link" )
                ,
                CONSTRAINT "unique_url" UNIQUE ( "link" ) );"""
        )
    conn.commit()

    print("Succes create db %s" % (name_db))

def get_kode_kategori(kategori):
    sql_select_kode_ktgr = """
                            SELECT "kode"
                                FROM "katagori"
                            WHERE "sub_kategori" = '%s';
                    """

    
    conn = sqlite3.connect('./db/tokped/referensi.db')  # You can create a new database by changing the name within the quotes
    curr = conn.cursor()
    
    sql = sql_select_kode_ktgr 
    curr.execute(sql % (kategori))
    
    result = curr.fetchone()
    #print(result[0][0],' %s'%(d))
    return result[0]

def sql_upload_prod(values, name_db):
    sql_template_product = '''
        INSERT INTO "template_tokped" ( "asuransi_pengiriman", "berat_gram", "deskripsi_produk", 
                                        "harga", "jumlah_stok", "kategori_kode", "kondisi", 
                                        "link", "link_gambar1", "link_gambar2", "link_gambar3", 
                                        "link_gambar4", "link_gambar5", "minimum_pesanan", 
                                        "nama_produk", "nomor_etalase", 
                                        "sku_name", "status", "url_video1", "url_video2", 
                                        "url_video3", "waktu_proses_po") 
            VALUES ( '%s', '%s', '%s', '%s', '%s', 
                '%s', '%s', '%s', '%s', '%s', '%s', 
                '%s', '%s', '%s', '%s', '%s', 
                '%s', '%s', '%s', '%s', '%s', '%s'
                );
        '''


    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    curr = conn.cursor()
    sql = sql_template_product % values
    curr.execute(sql)

    conn.commit()

    print('succes store db template_tokped : %s' % values[7])

def sql_insert_detProd(values, name_db):
    sql_detProd = '''
                    INSERT INTO "product_detail" ( "link", "n_discuss", "n_reviews", "n_stars", "n_stock", "n_terjual","tanggal") 
                            VALUES ( '%s', '%s', '%s', '%s', '%s', '%s','%s');
                    '''
    
   
    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    curr = conn.cursor()
    sql = sql_detProd % values
    curr.execute(sql)

    conn.commit()

    print('succes store db product_detail : %s' % values[0])

def sql_get_all_dataTemplate(name_db):
    sql = '''
            SELECT
                "nama_produk",
                "deskripsi_produk",
                "kategori_kode",
                "berat_gram",
                "minimum_pesanan",
                "nomor_etalase",
                "waktu_proses_po",
                "kondisi",
                "link_gambar1",
                "link_gambar2",
                "link_gambar3",
                "link_gambar4",
                "link_gambar5",
                "url_video1",
                "url_video2",
                "url_video3",
                "sku_name",
                "status",
                "jumlah_stok",
                "harga",
                "asuransi_pengiriman"
            FROM "template_tokped";
            '''

    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    curr = conn.cursor()

    curr.execute(sql)
    hasil = curr.fetchall()

    return hasil

def sql_get_alltokped(name_db):
    sql = '''
            SELECT
                *
            FROM "template_tokped";
            '''

    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    conn.row_factory = sqlite3.Row
    curr = conn.cursor()

    curr.execute(sql)
    hasil = [dict(row) for row in curr.fetchall()]
    # print(hasil)
    return hasil

def sql_insert_task_tokped(values, name_db):
    sql_task_tokped = '''
        INSERT INTO "task" ( "date", "upload","filename", "path", "status",workbook_path) 
                    VALUES ( DATE(), False, '%s', '%s', '%s','%s');
    '''
    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    curr = conn.cursor()
    sql = sql_task_tokped % values
    # print(sql)
    curr.execute(sql)

    conn.commit()

    print('succes store task tokped  : %s' % values[0])

def sql_get_task(name_db):
    sql_get_task = '''
            SELECT
                "markup",
                "path",
                "upload",
                "workbook_path"
            FROM "task"
            WHERE 
                "status" = 'FALSE' AND "markup">0;
    '''
    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    conn.row_factory = sqlite3.Row
    curr = conn.cursor()

    curr.execute(sql_get_task)
    hasil = [dict(row) for row in curr.fetchall()]

    return hasil

def sql_get_alltask(name_db='db/tokped/task.db'):
    sql_get_task = '''
            SELECT
                *
            FROM "task"
    '''
    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    conn.row_factory = sqlite3.Row
    curr = conn.cursor()

    curr.execute(sql_get_task)
    hasil = [dict(row) for row in curr.fetchall()]

    return hasil

def sql_get_edittask(values:tuple, name_db='db/tokped/task.db'):
    sql_edit_task = '''
            UPDATE "task" SET
                "markup" = '%s',
                "upload" = %s
            WHERE
                "path" = '%s';
    '''
    # print(values)
    conn = sqlite3.connect(name_db)  # You can create a new database by changing the name within the quotes
    curr = conn.cursor()
    sql = sql_edit_task % values
    # print(sql)
    curr.execute(sql)

    conn.commit()

    return 1

########################################################################################################################

sql_template_bl_ins = '''
        INSERT INTO "template_bl" 
                    ( "asuransi", "berat", "deskripsi", 
                    "harga", "kode_sku", "kondisi", 
                    "link", "nama_barang", "nama_barang", 
                    "pengiriman", "stok", "url_gambar1", 
                    "url_gambar2", "url_gambar3", 
                    "url_gambar4", "url_gambar5") 
        VALUES ( '%s', '%s', '%s', 
                '%s', '%s', '%s',
                '%s','%s', '%s',
                '%s','%s', '%s',
                '%s','%s',
                '%s','%s', );
        '''