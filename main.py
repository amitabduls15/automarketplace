p (371)
p (372)
p (373)
p (374)
p (375)
p (376)
p (377)
p (378(379)
p (380)import os
import time

from selenium import webdriver
from tqdm import tqdm
from datetime import datetime

from scrapper_tokped import scrape_terlaris, bulk_upload
from utils import mass_upload_img, Random_uid, PrintException, create_xlsx
from dbtools import create_db ,get_kode_kategori \
                    ,sql_upload_prod, sql_insert_detProd\
                    , sql_get_all_dataTemplate\
                    , sql_insert_task_tokped, sql_get_task

import schedule

static_path = str(os.getcwd())
# folder_download = static_path + '/output/tokopedia/'
folder_download = 'output/tokopedia/'

chrome_options = webdriver.ChromeOptions()
# myProxy = '175.106.17.62:47641'
chrome_options.add_argument("user-data-dir=cache/selenium")
# chrome_options.add_argument('--proxy-server=%s' % myProxy) 
# chrome_options.add_argument('--headless') #for pop up windows
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-setuid-sandbox')

chrome_options.add_experimental_option("prefs", {
  "download.default_directory": r"{}".format(folder_download),
  "download.prompt_for_download": False,
  "download.directory_upgrade": True,
  "safebrowsing.enabled": True
})



def job(options=chrome_options,static_path=static_path):
    TASK_DB = 'db/tokped/task.db'
    tanggal = str(datetime.now().strftime("%d-%m-%Y"))
    name_db =  'db/tokped/'+ tanggal +'.db'
    workbook_path =  'example/template_tokped.xlsx'
    output_path =  os.path.join(static_path,'output/tokopedia/tokopedia_' + tanggal + '.xlsx')
    
    value_task_tokped = (
        name_db.split('/')[-1], name_db, False, output_path
    )

    key = 'd16cf2dab13d42392e2793fb5df27190'

    if not os.path.exists(name_db):
        create_db(name_db=name_db)

        random_uid = Random_uid()
        hasil = scrape_terlaris(options = options, root='./image/')

        
        for data in tqdm(hasil, desc='uploading to db'):
            try:
                list_img = data['list_img']
                list_link = mass_upload_img(list_img=list_img, key=key)
                print(type(data['product_name']))
                print(data['product_name'])

                value_tmpt_prodct = (
                        'opsional', data['Berat'], data['description'],
                        data['product_price'], data['n_stock'], get_kode_kategori(data['Kategori']), data['Kondisi'],
                        data['link'],list_link[0], list_link[1], list_link[2], list_link[3], list_link[4],
                        1 , data['product_name'], 'NULL', random_uid.get_sku_name(), 'Aktif', 'NULL','NULL','NULL','NULL'
                    )
                value_detprod =(
                                data['link'], data['n_discuss'], data['n_reviews'] , data['n_stars'], data['n_stock'], data['n_terjual'],str(datetime.now())
                            )
                # print(value_detprod)
                sql_upload_prod(values=value_tmpt_prodct, name_db=name_db)
                sql_insert_detProd(values=value_detprod, name_db=name_db)
                
            except:
                PrintException()
                continue
    try:
        sql_insert_task_tokped(values= value_task_tokped, name_db=TASK_DB)
    except:
        PrintException()
        pass

    task_s = sql_get_task(name_db=TASK_DB)
    # print(task)
    for task in task_s:
        name_db = task['path']
        output_path = task['workbook_path']
        markup = task['markup']
        upload = task['upload']

        print('\nCreating Excell Template Tokopedia\n')
        data_product = sql_get_all_dataTemplate(name_db=name_db)
        create_xlsx(workbook_path=workbook_path,data_product=data_product,output_path=output_path ,markup=markup)

        if upload:
            print('Uploading bluk data tokopedia')
            bulk_upload(options=options, path_data=output_path)    

schedule.every().hour.do(job)

if __name__ == '__main__':
    job()
    while True:
        schedule.run_pending()
        time.sleep(1)
